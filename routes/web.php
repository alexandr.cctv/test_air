<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('login', [
    'as' => 'loginForm',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@login'
]);

Route::get('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);
Route::get('/register', [
    'as' => 'registerForm',
    'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('/register', [
    'as' => 'register',
    'uses' => 'Auth\RegisterController@register'
]);


Route::prefix('admin')->middleware(['auth', 'web'])->group(function () {
    Route::get('/', function () {
        return view('admin');
    });
    Route::get('/dashboard', [
        'as' => 'dashboard',
        'uses' => 'Admin\DashboardController@index'
    ]);
    Route::get('/event/blogers/{id}', [
        'as' => 'eventEventBlogers',
        'uses' => 'Admin\DashboardController@getEventBlogers'
    ]);
    Route::post('/event/blogers/{id}', [
        'as' => 'eventEventBlogers',
        'uses' => 'Admin\DashboardController@changePosition'
    ]);

    Route::resources(
        [
            '/user' => 'Admin\UserController',
            '/event' => 'Admin\EventController',
            '/bloger' => 'Admin\BlogerController',
        ]
    );
});