<?php
/**
 * User: Alex Chesnui
 * Date: 26.05.18
 * Time: 12:42
 */
namespace App\Http\Controllers\Admin;

use App\Http\Resources\EventCollection;
use App\Models\Event;
use Carbon\Carbon;

/**
 * Class EventController
 *
 * @package App\Http\Controllers\Admin
 */
class EventController extends AdminControllerAbstract
{
    /**
     * Return formatted events
     *
     * @return \App\Http\Resources\EventCollection|\Illuminate\Http\JsonResponse
     */
    public function index()
    {
        setlocale(LC_TIME, 'ru_RU.utf8');
        $events = Event::orderByDesc('start_date')->get()->map(function ($item){
            $item->start_date_formatted = Carbon::parse($item->start_date)->formatLocalized('%B %Y');
            return $item;
        });
        setlocale(LC_TIME, '');

        return new EventCollection($events);
    }
}