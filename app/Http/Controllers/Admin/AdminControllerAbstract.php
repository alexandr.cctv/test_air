<?php
/**
 * User: Alex Chesnui
 * Date: 26.05.18
 * Time: 11:52
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ValidateModel;
use App\Models\ValidationRulesTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Finder\Finder;

/**
 * Class AdminControllerAbstract
 *
 * @package App\Http\Controllers\Admin
 */
abstract class AdminControllerAbstract extends Controller
{
    const MODELS_DIR = '/app/Models';
    const RESOURCES_DIR = '/app/Http/Resources';

    protected $modelClass;
    protected $model;

    /**
     * AdminControllerAbstract constructor.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $model
     */
    public function __construct(Request $request, string $model = '')
    {
        $this->model = $model;
        if (!$this->model) {
            $this->model = $this->getModelFromUrl($request);
        }

        if (!$this->model) {
            return $this->errorJsonResponse(404, 'Model not found!!!');
        }

        $this->modelClass = $this->findModel($this->model);

        if (!$this->modelClass) {
            return $this->errorJsonResponse(404, 'Model not found!!!');
        }
    }

    /**
     * Return all entities
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $resourceClass = $this->findResource($this->model);

        if (!$resourceClass) {
            return $this->errorJsonResponse(404, 'Resource not found!!!');
        }

        return new $resourceClass($this->modelClass::all());
    }

    /**
     * Edit entity
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(int $id)
    {
        $entity = $this->modelClass::find($id);

        if (!$entity) {
            return $this->errorJsonResponse(404, 'Item not found!!!', $id);
        }

        $modelSchema = $this->getTableStructure($entity);

        return response()->json(
            [
                'data' => $entity,
                'schema' => $modelSchema,
            ]
        );
    }

    /**
     * Delete entity
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        $entity = $this->modelClass::find($id);

        if (!$entity) {
            return $this->errorJsonResponse(404, 'Item not found!!!', $id);
        }

        if ($entity->delete()) {
            return $this->successDestroyJsonResponse($id);
        }

        return $this->errorJsonResponse(404, 'Delete fail!!!', $id);
    }

    /**
     * Update entity
     *
     * @param int                      $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, Request $request)
    {
        $entity = $this->modelClass::find($id);

        if (!$entity) {
            return $this->errorJsonResponse(404, 'Item not found!!!', $id);
        }

        foreach ($request->all() as $columnName => $value) {
            $entity->$columnName = $value;
        }

        if ($entity->save()) {
            return $this->successJsonResponse($id);
        }

        return $this->errorJsonResponse(404, 'Update fail!!!', $id);
    }

    /**
     * Find resource
     *
     * @param string $modelName
     *
     * @return null|string
     */
    protected function findResource(string $modelName)
    {
        $resorceName = $modelName.'Collection';
        $this->findFiles($finder, self::RESOURCES_DIR, $resorceName);
        foreach ($finder as $file) {
            $namespace = $this->getNamespaceFromContent($file->getContents());
            if ($namespace) {
                return $namespace.$resorceName;
            }
        }

        return null;
    }

    /**
     * Find model class
     *
     * @param string $modelName
     *
     * @return null|string
     */
    protected function findModel(string $modelName): ?string
    {
        $this->findFiles($finder, self::MODELS_DIR, $modelName);

        foreach ($finder as $file) {
            $namespace = $this->getNamespaceFromContent($file->getContents());
            if ($namespace) {
                return $namespace.$modelName;
            }
        }

        return null;
    }

    /**
     * Find files
     *
     * @param \Symfony\Component\Finder\Finder $finder
     * @param string                           $dir
     * @param string                           $modelName
     */
    private function findFiles(&$finder, string $dir, string $modelName)
    {
        $finder = new Finder();
        $finder->files()->in(app()->basePath().$dir)->name(
            $modelName.".php"
        );
    }

    /**
     * Return namespace from file content
     *
     * @param string $content
     *
     * @return null|string
     */
    private function getNamespaceFromContent(string $content): ?string
    {
        preg_match_all(
            '/namespace (.*)\;/m',
            $content,
            $matches,
            PREG_SET_ORDER,
            0
        );

        if (isset($matches[0]) && isset($matches[0][1])) {
            return "\\".$matches[0][1]."\\";
        }

        return null;
    }

    /**
     * Return model from url
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return null|string
     */
    protected function getModelFromUrl(Request $request): ?string
    {
        $segments = $request->segments();

        if (count($segments) < 2) {
            return null;
        }
        $result = explode('-', ucfirst($segments[1]));

        if (count($result) == 1) {
            return $result[0];
        }

        foreach ($result as $key => $value) {
            $result[$key] = ucfirst($value);
        }

        return implode('', $result);

    }

    /**
     * Return entity table structure
     *
     * @param \Illuminate\Database\Eloquent\Model $entity
     *
     * @return array
     */
    private function getTableStructure(Model $entity): array
    {
        $schema = DB::connection()->getDoctrineSchemaManager();
        $columns = $schema->listTableColumns($entity->getTable());
        $modelSchema = [];
        $modelFillableFields = $entity->getFillable();
        $validationRules = (in_array(ValidationRulesTrait::class, class_uses($entity)))?$entity->getValidationRulesAsStrings():[];

        foreach ($columns as $column) {
            $colName = $column->getName();
            if (!in_array($colName, $modelFillableFields)) {
                continue;
            }
            $rules = '';

            if (array_key_exists($colName, $validationRules)) {
                $rules = $validationRules[$colName];
            }
            $modelSchema[$colName] = [
                'name' => $colName,
                'type' => (string)$column->getType(),
                'rules' => $rules
            ];
        }

        return $modelSchema;
    }

    /**
     * Return error response
     *
     * @param int    $code
     * @param string $message
     * @param int    $id
     * @param string $model
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function errorJsonResponse(
        int $code,
        string $message = 'Undefine error',
        int $id = 0,
        $model = ''
    ) {
        $responseData = [
            'message' => $message
        ];

        if ($id) {
            $responseData['id'] = $id;
        }

        if ($model) {
            $responseData['model'] = $model;
        }

        return response()->json($responseData)->setStatusCode($code);
    }

    /**
     * Destroy success json response
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function successJsonResponse(int $id)
    {
        return response()->json(
            [
                'id' => $id,
            ]
        );
    }
}