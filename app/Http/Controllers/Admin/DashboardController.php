<?php
/**
 * User: Alex Chesnui
 * Date: 26.05.18
 * Time: 15:12
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bloger;
use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController
 *
 * @package App\Http\Controllers\Admin
 */
class DashboardController extends Controller
{
    const CHANGE_TYPE_UP = 'up';

    /**
     * Return events by formatted date
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        setlocale(LC_TIME, 'ru_RU.utf8');
        $events = Event::orderByDesc('start_date')->selectRaw(
            'DATE_FORMAT(start_date, \'%Y/%m\') as date, events.*'
        )->get()->map(
            function ($item) {
                $item->start_date_formatted = Carbon::parse(
                    $item->start_date
                )->formatLocalized('%B %Y');

                return $item;
            }
        );

        return response()->json($events->groupBy('date')->toArray());
    }

    public function getEventBlogers($id)
    {
        $blogers = collect(
            Event::find($id)->blogers->map(
                function ($item) {
                    return [
                        'id' => $item->id,
                        'name' => $item->name,
                        'avatar_url' => $item->avatar_url,
                        'serial_number' => $item->pivot->serial_number,
                        'pivot_id' => $item->pivot->id,
                    ];
                }
            )
        )->sortBy('serial_number');

        return response()->json(
            [
                'data' => $blogers,
                'columns' => [
                    'id',
                    'name',
                    'avatar_url',
                    'serial_number',
                ],
                'sort_by' => 'serial_number',
            ]
        );
    }

    public function changePosition(\Illuminate\Http\Request $request)
    {
        $blogerId = $request->get('bloger_id');
        $blogers = Event::find($request->get('event_id'))->blogers()->orderBy(
            'serial_number'
        )->get();
        $type = $request->get('type');
        foreach ($blogers as $key => $bloger) {
            $bloger->pivot->serial_number = $key;
            $bloger->pivot->save();
            if ($bloger->id == $blogerId && $type === self::CHANGE_TYPE_UP) {
                $prev = $blogers->get($key - 1);
                $prev->pivot->serial_number = $key;
                $prev->pivot->save();
                $bloger->pivot->serial_number = $key - 1;
                $bloger->pivot->save();
            }
            $prev = $blogers->get($key - 1);
            if (($type !== self::CHANGE_TYPE_UP) && ($prev && $prev->id == $blogerId)) {
                $prev->pivot->serial_number = $key;
                $prev->pivot->save();
                $bloger->pivot->serial_number = $key - 1;
                $bloger->pivot->save();
            }
        }

        return response('', 200);
    }
}