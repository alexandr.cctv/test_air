<?php
/**
 * User: Alex Chesnui
 * Date: 26.05.18
 * Time: 12:51
 */
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class BlogerCollection
 *
 * @package App\Http\Resources
 */
class BlogerCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];
    }
}
