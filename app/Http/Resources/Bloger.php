<?php
/**
 * User: Alex Chesnui
 * Date: 26.05.18
 * Time: 12:51
 */
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Bloger
 *
 * @package App\Http\Resources
 */
class Bloger extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}