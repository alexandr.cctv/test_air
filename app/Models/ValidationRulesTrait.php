<?php
/**
 * User: Alex Chesnui
 * Date: 26.05.18
 * Time: 15:50
 */
namespace App\Models;

/**
 * Trait ValidationRulesTrait
 *
 * @package App\Models
 */
trait ValidationRulesTrait {
    /**
     * Return rules
     *
     * @return array
     */
    public function getValidationRules(): array
    {
        return $this->validationRules;
    }

    /**
     * Return rules in vue validate format
     *
     * @return array
     */
    public function getValidationRulesAsStrings(): array
    {
        $rules = [];
        foreach ($this->validationRules as $key => $rule) {
            if (!is_array($rule)) {
                $rules[$rule] = '';
            } else {
                $rules[$key] = implode('|', $rule);
            }
        }
        return $rules;
    }
}