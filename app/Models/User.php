<?php
/**
 * User: Alex Chesnui
 * Date: 26.05.18
 * Time: 12:48
 */
namespace App\Models;

/**
 * Class User
 *
 * @package App\Models
 */
class User extends \App\User
{
    protected $fillable = [
        'name',
        'email'
    ];
}