<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Event
 *
 * @package App\Models
 */
class Event extends Model
{
    use ValidationRulesTrait;

    protected $table = 'events';

    protected $fillable = [
        'name',
        'start_date'
    ];

    protected $validationRules = [
        'name' => [
            'required'
        ]
    ];

    protected $dates = [
      'start_date'
    ];

    /**
     * Return event blogers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blogers()
    {
        return $this->belongsToMany(Bloger::class, 'event_bloger')->withPivot('id', 'serial_number')->withTimestamps();
    }
}
