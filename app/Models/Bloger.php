<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bloger
 *
 * @package App\Models
 */
class Bloger extends Model
{
    protected $fillable = [
        'name',
        'avatar_url'
    ];

    /**
     * Return bloger events
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany(Event::class, 'event_bloger')->withPivot('id', 'serial_number')->withTimestamps();
    }
}
