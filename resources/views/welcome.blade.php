@extends('main')
@section('content')
    <section class="section">
        <div class="container has-text-centered">
            <h1 class="title">
                TEST APP
            </h1>
            <p class="subtitle">
                APP for test task
            </p>
        </div>
    </section>
    <section class="section">
        <div class="columns is-mobile is-centered">
            <div class="column is-3 is-narrow has-text-centered">
                <aside class="menu">
                    <p class="menu-label">
                        You can do:
                    </p>
                    <ul class="menu-list">
                        @auth
                            <li><a href="{{ url('/admin') }}">Admin panel</a></li>
                            <li><a href="{{ route('logout') }}">Logout</a></li>
                        @else
                            <li><a href="{{ route('loginForm') }}">Login</a></li>
                            <li><a href="{{ route('registerForm') }}">Register</a></li>
                        @endauth
                    </ul>
                </aside>
            </div>
        </div>
    </section>
@endsection