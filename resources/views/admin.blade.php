<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin panel</title>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
<div id="app">
    <nav class="navbar is-transparent">
        <div class="navbar-brand">
            <a class="navbar-item"
               href="https://www.linkedin.com/in/oleksandrpidmogylnyi/">
                <img src="{{url('img/logo.jpeg')}}"
                     width="110" height="138" style="height: 138px">
            </a>
            <div class="navbar-burger burger"
                 data-target="navbarExampleTransparentExample">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="navbar-end">
            <div class="navbar-item">
                <div class="field is-grouped">
                    <p class="control">
                        <a class="button is-primary"
                           href="{{ route('logout') }}">
              <span class="icon">
                <i class="fas fa-sign-out-alt"></i>
              </span>
                            <span>Logout</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </nav>
    <section class="section">
        <div class="columns">
            <div class="column is-one-fifth">
                <left-menu></left-menu>
            </div>
            <div class="column">
                <router-view></router-view>
            </div>
        </div>
    </section>
</div>

<script src="{{mix('/js/app.js')}}"></script>
</body>
</html>