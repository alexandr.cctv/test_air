export default {
    data: function () {
        return {
            model: null,
            modelId: null,
            responseData: {},
        }
    },
    created: function(){
        this.getTableStructureAndData();
    },
    watch: {
        '$route': 'getTableStructureAndData'
    },
    methods: {
        getTableStructureAndData: function () {
            this.modelId = this.$route.params.id;
            this.model = this.$router.currentRoute.path;
            axios.get('/admin' + this.model + '/edit')
                .then(response => {
                    this.responseData = response.data;
                    this.prepareInputs();
                })
                .catch(function(error){
                    console.error(error);
                });
        },
        prepareInputs: function()
        {
            for (let item in this.responseData.schema) {
                this.responseData.schema[item].tag = this.setTag(this.responseData.schema[item].type);
                this.responseData.schema[item].tagType = this.setType(this.responseData.schema[item].type);
                this.responseData.schema[item].hasError = false;
            }
        },
        setTag: function(value)
        {
            switch (value) {
                case 'Text':
                    return 'textarea';
                default:
                    return 'input';
            }
        },
        setType: function(valueType)
        {
            switch (valueType) {
                case 'Integer':
                    return 'number';
                default:
                    return 'text';
            }
        },
        storeChanges: function()
        {
            let data = {};
            for (let item in this.responseData.schema) {
                data[item] = this.responseData.data[item];
            }
            axios.put('/admin' + this.model, data)
                .then(response => {
                    alert('Successful saved');
                })
                .catch(function(error){
                    alert(error.response.data.message);
                    console.error(error);
                });
        }
    }
}