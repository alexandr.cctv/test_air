export default {
    data: function () {
        return {
            model: null,
            responseData: null,
            tableData: [],
            columns: []
        }
    },
    created: function () {
        this.fetchData();
    },
    watch: {
        '$route': 'fetchData'
    },
    methods: {
        fetchData: function () {
            axios.get('/admin/dashboard')
                .then(response => {
                    this.responseData = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        showBlogers: function(eventId){
            this.$router.push({ name: 'event_blogers', params: { id: eventId }});
        }
    }
}