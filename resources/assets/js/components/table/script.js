export default {
    data: function () {
        return {
            model: null,
            responseData: null,
            tableData: [],
            columns: []
        }
    },
    created: function () {
        this.fetchData();
    },
    watch: {
        '$route': 'fetchData'
    },
    methods: {
        fetchData: function () {
            this.model = this.$router.currentRoute.path;
            axios.get('/admin' + this.model)
                .then(response => {
                    this.responseData = response.data;
                    console.log(this.responseData);
                    this.tableData = response.data.data;
                    this.setColumns();
                    if (this.responseData.hasOwnProperty('sort_by')) {
                        this.sortData(this.responseData.sort_by);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        deleteRow: function (id) {
            axios.delete('/admin' + this.model + '/' + id)
                .then(response => {
                    this.tableData = this.tableData.filter(el => el.id !== response.data.id);
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        setColumns: function () {
            if (this.responseData === null) {
                return;
            }

            if (this.responseData.hasOwnProperty('columns')) {
                this.columns = this.responseData.columns;
                return;
            }

            if (!this.tableData.hasOwnProperty(0)) {
                console.error('No data!!!');
            }

            this.columns = Object.keys(this.tableData[0]);
        },
        upPosition: function (blogerId) {
            console.log(this.$route);
            axios.post('/admin' + this.model + '/', {
                type: 'up',
                bloger_id: blogerId,
                event_id: this.$route.params.id
            })
                .then(response => {
                    this.fetchData();
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        downPosition: function (blogerId) {
            axios.post('/admin' + this.model + '/', {
                type: 'down',
                bloger_id: blogerId,
                event_id: this.$route.params.id
            })
                .then(response => {
                    this.fetchData();
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        sortData: function (columnName = 'id') {
            for (let item in this.responseData.data) {
                if (!this.responseData.data[item].hasOwnProperty(columnName)) {
                    return;
                }

                if (this.responseData.data[Number(item) + 1] === undefined) {
                    continue;
                }

                //
                if (this.responseData.data[item][columnName] > this.responseData.data[Number(item) + 1][columnName]) {
                    let temp = this.responseData.data[item];
                    this.responseData.data[item] = this.responseData.data[Number(item) + 1];
                    this.responseData.data[Number(item) + 1] = temp;
                    this.sortData(columnName);

                    break;
                }
            }
        }
    },
    filters: {
        subLongText: function (str) {
            if (str === null) {
                return '';
            }

            if (str.length > 20) {
                return str.substr(0, 20) + '...';
            }

            return str;
        }
    }
}