export default {
    data: function () {
        return {
            menuItems: []
        }
    },
    created: function () {
        for (let route in this.$router.options.routes) {
            if (this.$router.options.routes[route].menu) {
                this.menuItems.push({
                    path: this.$router.options.routes[route].path,
                    name: this.$router.options.routes[route].name,
                    is_active: this.isCurrentRoute(this.$router.options.routes[route].path)
                });
            }
        }
    },
    methods: {
        click: function(menuName){
            for (let item in this.menuItems) {
                if (this.menuItems[item].name === menuName) {
                    this.menuItems[item].is_active = true;
                } else {
                    this.menuItems[item].is_active = false;
                }
            }
        },
        isCurrentRoute: function(path){
            if (this.$route.path === path) {
                return true;
            }
            return false;
        }
    },
    filters: {
        ucFirst: function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
    }
}