import VueRouter from 'vue-router';
import Dashboard from '../components/dashboard/Dashboard';
import Table from '../components/table/Table';
import EditForm from '../components/edit/Edit';

const routes = [
    {path: '/', menu: true, name: 'dashboard', component: Dashboard},
    {path: '/user', menu: true, name: 'users', component: Table},
    {path: '/user/:id', menu: false, component: EditForm},
    {path: '/event', menu: true, name: 'events', component: Table},
    {path: '/event/:id', menu: false, component: EditForm},
    {
        path: '/event/blogers/:id',
        menu: false,
        name: 'event_blogers',
        component: Table,
    },
    {path: '/bloger', menu: true, name: 'blogers', component: Table},
    {path: '/bloger/:id', menu: false, component: EditForm},
];

const router = new VueRouter({
    routes
});

export default router;