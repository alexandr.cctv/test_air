#!/usr/bin/env bash
chmod 777 storage/framework/views/
chmod 777 storage/framework/sessions/
chmod 777 storage/framework/cache/
chmod 777 bootstrap/cache/
chmod 777 storage/logs/
chmod 777 vendor/
cp .env.example .env
docker-compose build
docker-compose up -d
docker-compose exec --user www-data app composer install -d /app -v
docker-compose exec app php /app/artisan migrate --seed
docker-compose exec app php /app/artisan key:generate

echo "app url http://172.10.10.2:8888"

echo "test login: test@test.com"
echo "test password: secret"