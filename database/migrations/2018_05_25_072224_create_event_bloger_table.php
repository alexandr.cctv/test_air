<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventBlogerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'event_bloger',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('event_id', false, true);
                $table->integer('bloger_id', false, true);
                $table->integer('serial_number', false, true)->default(0);
                $table->timestamps();

                $table->foreign('event_id')->references('id')->on(
                    'events'
                )->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('bloger_id')->references('id')->on(
                    'blogers'
                )->onUpdate('cascade')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_bloger');
    }
}
