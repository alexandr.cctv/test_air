<?php

use Illuminate\Database\Seeder;

/**
 * Class EventsSeeder
 *
 */
class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Event::class, 5)->create()->each(function ($event) {
            for ($i = 0; $i < 5; $i++ ) {
                $event->blogers()->save(factory(\App\Models\Bloger::class)->make());
            }

            foreach ($event->blogers as $key => $bloger ) {
                $bloger->pivot->serial_number = $key;
                $bloger->pivot->save();
            }
        });
    }
}
