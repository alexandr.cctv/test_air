<?php

use Illuminate\Database\Seeder;

class BlogersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Bloger::class, 15)->create()->each(function ($bloger) {
            $bloger->events()->save(factory(\App\Models\Event::class)->make());
        });
    }
}
