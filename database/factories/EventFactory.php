<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Event::class, function (Faker $faker) {
    return [
        'name' => $faker->numerify('Event###'),
        'start_date' => $faker->date('Y-m-d'),
    ];
});
