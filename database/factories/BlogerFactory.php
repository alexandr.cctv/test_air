<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Bloger::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'avatar_url' => $faker->imageUrl(100, 100),
    ];
});
